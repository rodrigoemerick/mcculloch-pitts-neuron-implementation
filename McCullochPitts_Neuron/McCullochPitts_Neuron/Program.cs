﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace McCullochPitts_Neuron
{
    class Program
    {
        static void Main(string[] args)
        {
            Neuron m_Neuron = new Neuron(3);
            Console.WriteLine($"{m_Neuron.Output}");
        }
    }

    class Neuron
    {
        int m_NumberOfInputs;
        Random m_Random;
        double m_Bias;
        double[] m_Inputs;
        double[] m_InputsWeights;

        int m_Output;
        public int Output { get => m_Output; }

        double m_ResultSumForActivation;

        public Neuron(int numberOfInputs)
        {
            m_NumberOfInputs = numberOfInputs;

            m_Random = new Random((int)DateTime.UtcNow.Ticks);
            m_Bias = m_Random.NextDouble();

            m_Inputs = new double[m_NumberOfInputs];
            m_InputsWeights = new double[m_NumberOfInputs];

            for (int i = 0; i < m_NumberOfInputs; ++i)
            {
                m_Inputs[i] = m_Random.NextDouble();
                m_InputsWeights[i] = m_Random.NextDouble();
            }

            CalcNeuron();
        }

        void CalcNeuron()
        {
            m_ResultSumForActivation = SumInputs();
            m_Output = DegreeActivationFunction(m_ResultSumForActivation);
        }

        double SumInputs()
        {
            double sum = 0.0;

            for (int i = 0; i < m_NumberOfInputs; ++i)
            {
                sum += m_Inputs[i] * m_InputsWeights[i];
            }

            sum += m_Bias;

            return sum;
        }

        int DegreeActivationFunction(double numberToCompare)
        {
            if (numberToCompare >= 1.0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}